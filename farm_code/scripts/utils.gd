extends Node

const GRID_SIZE := Vector2(800, 600)
const CELL_SIZE := Vector2(16, 16)
const CELLS_AMOUNT := Vector2(GRID_SIZE.x / CELL_SIZE.x, GRID_SIZE.y / CELL_SIZE.y)

const BLACK := Color("23213D")
const GRAY := Color("B9B5C3")
const WHITE := Color.WHITE
