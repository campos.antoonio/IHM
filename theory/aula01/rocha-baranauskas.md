# (Pág 1-23)

# O que é Interação Humano-Computador

# Interface Humano-Computador

Para que o usuário tenha mais poder, é preciso sim, que mais funcionalidades sejam oferecidas, mas é fundamental a facilidade de uso.

# Uma primeira definição de interfaces

Se visualiza uma interface como um lugar onde o contato entre duas entidades ocorre (tela de um computador, maçaneta, direção de um caro etc.)

A froma das interfaces reflete as qualidades físicas das partes na interação.

1° - Interface é uma superfície de contato que reflete as propriedades físicas das partes que interagem, as funções a serem executadas e o balanço entre poder e controle.

# Evolução de Interfaces e sua conceituação

- 1945-1955 (Pioneira): Válvulas, máquinas enormes e com alta ocorrência de falha.
- 1955-1956 (Histórica): Transistores, mais confiáveis. Computadores começaram a ser usados fora de laboratórios.
- 1965-1980 (Tradicional): Circuito integrado. Realação custo benefício justifica a compra de computadores para muitas necessidades.
- 1980-1995 (Moderna): VLSL. Pessoas podem comprar seu computador.
- 1995- (Futura): Integração de alta-escala. Pessoas podem comprar diversos computadores.

# Metáforas de Interface

As metáforas funcionam como modelos naturais, nos permitindo usar conhecimento familiar de objetos concretos e experiências para dar estrutura a conceitos mais abstratos. Em interfaces, imagine o ato de arrastar um arquivo de uma página para outra, na prática o que acontece é apenas que o _apontador_ para o arquivo mudou. Mas o usuário acredito que de fato mudou algo de uma "pasta" para outra.

Nesses casos, metáforas servem como auxiliares ao entendimento atuando como mediadores cofnitivos cujos rótulos são menos técnicos que os do jargão comptacional.

Não se pode pensar em interfaces sem considerar o ser humano que vai usá-la, e portanto **interface e interação são conceitos que não podem ser establecidos ou analisados independentement**.

# Interação Humano-Computador (IHC) (1980)

Computadores devem ser projetados para as necessidades e capacidades de um groupo alvo.

Indústria adotou o conceito de _user-firendly_ interface, mas muitas vezes apenas como um atrativo de mercado.

**IHC é a disciplina preocupada com o design, avaliação e implementação de sistemas computacionais interativos para uso humano e com o estudo dos principais fenômenos ao redor deles.**

### Desafios

1. Como dar conta da rápida evolução tecnológica?
2. Como garantir que os design oferaçam uma boa IHC ao mesmo tempo que exploram o potencial e funcionalidade da nova tecnologia?

## Objetivos

Produzir sistemas usáveis, seguros e funcionais. Como melhorar a segurança, utiliadde, efetividade e usablidade de sistemas que incluem computadores.

**Aceitabilidade geral** dem um sistema é a combinação de sua aceitabilidade social e sua aceitabilidade prática.


