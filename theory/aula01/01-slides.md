# Interfaces: Tipos e Evoluções


### 1940:

- **Tecnologias de hardware**: Mecânica e eletromecânica. Sem retorno visual
- **Modo de operação**: Usado somente para cálculo
- **Programação**: Movimento de cabos e chaves
- **Usuários**: Os próprios inventores

Exemplos: ENIAC, MARK1, IBM SSEC

### 1950-1960

- **Tecnologias de hardware**: Válvulas e transistores. Uso de painéis como retorno visual
- **Modo de operação**: Um usuário pro vez utiliza a máquina 
- **Programação**: Linguagem de máquina e assembly
- **Usuários**: Pioneros e profissionais de computação
- **Paradigma**: Programação em _batch_

Exemplo: Painel de controle strech

### 1960-1980

- **Tecnologias de hardware**: Circuito integrado. Monitores monocromáticos
- **Modo de operação**: Time sharing
- **Programação**: Linguagens de alto nível
- **Usuários**: Surgem os primeiros usuários utilizando computador para atividades repetitivas
- **Paradigma**: Menus hierárquicos e formulários

Exemplo: invenção do mouse, dos monitores

### 1980

- **Tecnologias de hardware**: Monitores coloridos tornam a interface mais agradável
- **Modo de operação**: Computador pessoal
- **Programação**: Linguagens orientadas a problemas. Surgimento de ferramentas para construção de interfaces
- **Usuários**: Todos os tipos de profissionais
- **Paradigma**: Menus hierárquicos e formulários

Exemplos: Surgimento das primeiras GUIs, Macintosh, windows

### 1990-2000

- **Tecnologias de hardware**: Computadores portáteis, redes de computadores, telas sensíveis ao toque.
- **Modo de operação**: Usuários conectados a redes, sistemas distribuídos
- **Programação**: Linguagens orientadas a objetos
- **Usuários**: Todos as pessoas
- **Paradigma**: Web user interface, Mais GUIs

Exemplos: Surgimento do primeiro navegador 

## Gerações de Interfaces

#### Walker

Redefine a geração dos computadores sob o ponto de vista de como os usuários interagem com ele em cinco gerações

###### Primeira geração

- Painéis com plugues
- Desenvolvidas para problemas específicos
- Usuário era um a um com o computador. Não havia mediação entre o computador e o usuário.

###### Segunda geração

- Lotes de cartões perfurados
- Introduzio novas formas de mediação e abstrações entre usuário e hardware
- Usuário ganha autonomia de tempo e não precisa ficar o tempo inteiro operando o computador

###### Terceira geração

- Tempo compartilhado via teletipo
- Execução concorrentes de múltiplos serviços
- Surgimento do conceito de produtividade do usuário

###### Quarta geração

- Sistemas de menus
- Desenvolvimento de terminais alnuméricos
- Menus de escolha

###### Quinta geração

- Controles gráficos e janelas
- Dispositivo apontador (mouse)

## Pressman

#### Primeira geração

- comandos e interfaces de perguntas
- comunicação puramente textual

#### Segunda geração

- Surgimento de menus

#### Terceira geração

- Melhoria na comunicação (orientada a janelas)
- Interfaces WIMP
- Mesa de trabalho (desktop)

#### Quarta geração

- Hipertexto e multitarefa


