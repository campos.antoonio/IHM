# Introdução

## Definição de interface

- Genérica: Superfície entre duas faces; Lugar onde acontece o contato entre duas entidades: Maçanetas de portas, torneiras, etc.
- Informática: Parte do sistema computacional com o qual o usuário entre em contato físico e perceptivo.

## Expectativas

- Quando interagimos com objetos conhecidos, esperamos um comportamento baseado em experiências passadas

## Usuários

- Trazem consigo experiências passadas
- Se ele não sabe muito a respeito de um objeto ou tarefas, irá associar com algo que já é de seu conhecimento
- Usuários têm necessidades diferentes em função da sua experiência (diferentes perfis)

## Interface homem-máquina

- Interface é o software responsável por mapear ações do usuário em solicitações de processamento apara a aplicação, bem como apresentar resultados produzidos pelo sistema.

## Características em uma interface

- Diversidade: adpatar-se ao usuário
- Complacência: prever erros do usuário
- Eficiência: quanto menos esforço, melhor
- Satisfação
- Consistência: manter o mesmo modelo em todas as telas
- Prestimosidade: ajudar o usuário quando este não souber o que fazer
- Naturalidade: comunicação deve contar apenas elementos simples
- Flexibilidade: A interface deve fornecer várias maneiras para o usuário executar uma determinada tarefa
- Imitação: Deve-se comunicar com o usuário através de analogias, exemplos, comparações.

## Tipos de interfaces

- CUI (Character-based User Interface): Interfaces fundamentadas em textos e caracteres alfanuméricos.
- GUI: Interfaces fundamentas em gráficos e desenhos. Uso de metáforas de mesas de trabalho, documentos, botões, janelas
- WUI: Usadas na web (navegadores)
- VRUI: Interfaces fundamentadas no paradigma da realidade virtual

## Interação Humano-Computador

**Interação** é o que acontece entre o ser humano e o computador, ou seja, a **comunicação** entre duas entidades.

## Objetivos de IHC

- Produzir sistemas fáceis de utilizar, seguros e funcionais



