# 13 - 20

# Introdução

Interação humano-computador foi criada nos anos 80 por meio de instituições de pesquisa que desejavam permitir aos usuários fazerem uso dos sistemas computacionais que estavam e ainda estariam disponíveis.

### Humano

_inputs__ e _otputs_ ocasionados pela ação humana ao se relacionarem com o sistema computacional

### Computacional

Atividades por trás dos sistemas não são pertinentes ao usuário, portanto, uma interface rápida e amigável é um importante marco para a melhor entrega ao usuário final.

### Interação

Atividade de transpor de um conjunto para outro. Portanto é o ambiente entre humano e computacional

- Contato físico: informação ou ação é transmitida por mei de um dispositivo de hardware para o sistema. (Mouses, teclados, webcam...)
- Contato conceitural: Usuário interpreta, processa e raciocina

<Fig 1>

Durante a interação as entidades usuários e sistema realizam alternância de dois papéis: de quem **fala** e o de quem **escuta**. Assim a interação é o canal que realiza a troca de informações entre o usuário e o sistema computacional.

<Fig 2>

- Pela ação do usuário

    - clicar em um botão
    - abrir um relatório
    - incluir um novo cadastro

- Pela ação do sistema

    - exibir relatório
    - avisar sobre uma inconsistência
    - lembrar usuário sobre compromisso

### Indentificando as áreas relacionadas à IHC

- Psicologia
- Socialogia
- Computação
- Ergonomia
- ...



