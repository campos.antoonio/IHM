# 1 - Qual a diferença entre wireframe, mockup e protótipo?

## wireframe: 

Apresenta o design básico de layout de um ponto de vista de baixa fidelidade ao produto final a fim de adquirir feedback. Sendo assim, possue baixa fidelidade, não apresenta funcionalidades e gasta-se pouco tempo para fazê-lo.

## mockup: 

Ao contrário do wireframe, o mockup apresenta o layout com mais alta fidelidade a fim de demonstrar como o produto final irá parece-se. Portanto, possuem mais alta fidelidade, ainda não apresenta funcionalidade e tem um tempo relativamente médio de desenvolvimento.

## prototype:

Diferentes dos dois anteriores o protótipo tem um foco na funcinalidade a fim de ajudar os stakeholders entenderem o fluxo da aplicação e validarem-a. Ele possui alta fidelidade, apresenta as funcionalidades, tempo maior para desenvolvimento. 


# 2 - Descreva e exemplique os componentes de Nielsen

- facilidade de aprendizado: A facilidade é analisada em função do tempo que o usuário leva para atingir algum proveito para realizar suas tarefas. Sendo assim, a facilidade de uso significa que o usuário pode rapidamente começar a interagir e ser produtivo sem a necessidade de compreender tudo que a interface tem a dispor. 
- eficiência de uso: Refere-se a capacitade do sistema de tornar os seus usuários mais produtivo uma vez que estes tenham aprendido a usá-lo por um certo tempo.
- facilidade de memorização: Lida com a questão da interface ser facilmente relembrada, para caso o usuário volte a usar o sistema depois de algum tempo não seje necessário reabrendê-la.
- erros: O sistema deve ter a penas uma pequena taxa de erros, e caso algum ocorra a recuperação deve ser fácil sem ocorrer alguma perdar de trabalho
- satisfação subjetiva: O sistema deve ser agradável a fim de que os seus usuários tenham satisfação ao usá-lo, em outras palavras que gostem dele.

# 3 - 

- Visibilidade: O design deve sempre informar os usuários sobre o que está acontecendo por meio de feedbacks a certo intervalo de tempo
- Correspondência entre o sistema e o mundo real: O design deve falar a linguagem do usuário sem jargões técnicos
- Liberdade de controle fácil para o usuário: Uma vez que usuários podem tomar acções por acidente, a interface devem ter uma "saída de emergência" para cancelar a ação.

