[ATV_01] Aprentar trabalhos recentes que abordem um dos estilos de interação.

# An evaluation of augmented reality-based user interface in the design process

Aplicação para ser usada por arquitetos. A aplicação que roda em um tablet dispõem de uma interface que permite que permite ao designer criar formas geométricas como cubos, esferas cilindros (com diferentes core) ou adicionar modelos prontos. É possível mover, redimensionar e rotacionar os objetos. Permite ao arquiteto fazer o design de diferentes angulos.

Os usuários interagem com objetos no ambiente digital por meio de interfaces. Com o desenvolvimento de software e hardware, interfaces baseadas em representações gráficas tornaram-se possíveis para permitir diferentes abordagens dentro da IHC. Como resultado do uso generalizado de tecnologias como AR/VR, uma comunicação mais natural pode ser estabelecida com interfaces.

- A conexão do ambiente físico com o digital cria uma poderosa TAR
- A GUI foi usada no tablet

A realidade aumentada permite aumentar a percepção da realidade e criar ambientes artificiais.

Tipos de interfaces:

- Tangible User Interface: 

# Spatial User Interaction: What next?

A Spatial User Interaction refere-se a interações que resultam da consideração de atributos espaciais entre entidades para acionar funções do sistema. Suas principais características são:

- Distância: Quantidade de espaço que separa dois objetos 
- Orientação: Em qual direção um objeto está em relação a outro
- Localização: Indica localização da entidade
- Identidade: Conhecimento sobre a Identidade 


# An augmented Reality Interface for Human-Robot Unconstrained Environments

Uso de uma Augmented Reality Mobile Interface para interação com robôs manipuladores em superfícies planas. 

A interface de A.R permite localizar um robô manipulador em seu estado de trabalho. Usuários podem realizar tarefas de `pegar-e-mover` objetos sem a necessidade de um controlador dedicado.

A própria interface do smartphone substitui a necessidade de uma câmera de sensor externa e uma interface de hardware dedicado. Pelo próprio celular é possível ver a "visão" do robô.
